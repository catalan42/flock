(ns tst.demo.core
  (:use demo.core tupelo.core tupelo.test))

(dotest
  (is (ride-up? {:from 0 :to 3}))
  (isnt (ride-up? {:from 3 :to 0}))
  (isnt (ride-up? {:from 0 :to 0}))

  (is (ride-down? {:from 4 :to 0}))
  (isnt (ride-down? {:from 0 :to 3}))
  (isnt (ride-down? {:from 0 :to 0})))

(dotest
  (is (same-direction? (new-elevator {:rides-active #{{:from 0 :to 4}}}) {:from 3 :to 5}))
  (isnt (same-direction? (new-elevator {:rides-active #{{:from 0 :to 4}}}) {:from 3 :to 1})))


(dotest
  (init {:num-elevators 2
         :num-floors    5})
  ; (nl) (display-elevators)

  ; Two elevators parked on the ground floor
  (is= @elevators {0 {:id            0,
                      :state         :waiting,
                      :floor         0,
                      :rides-waiting #{},
                      :rides-active  #{}},
                   1 {:id            1,
                      :state         :waiting,
                      :floor         0,
                      :rides-waiting #{},
                      :rides-active  #{}}})

  ; E0 has a ride from floor 1 -> 4, and a synthetic ride to position from 0 -> 1
  (add-ride 0 1 4)
  ; (nl) (display-elevators)
  (is= @elevators {0 {:id            0,
                      :state         :working,
                      :floor         0,
                      :rides-waiting #{{:from 1, :to 4}},
                      :rides-active  #{{:from 0, :to 1}}},
                   1 {:id            1,
                      :state         :waiting,
                      :floor         0,
                      :rides-waiting #{},
                      :rides-active  #{}}})
  ; (nl) (display-elevators)

  (add-ride 1 3 2) ; (nl) (display-elevators)

  ; We added a ride to E1 from floor 3 -> 2. It has a synthetic ride to position from 0 to 3
  (is= @elevators {0 {:id            0,
                      :state         :working,
                      :floor         0,
                      :rides-waiting #{{:from 1, :to 4}},
                      :rides-active  #{{:from 0, :to 1}}},
                   1 {:id            1,
                      :state         :working,
                      :floor         0,
                      :rides-waiting #{{:from 3, :to 2}},
                      :rides-active  #{{:from 0, :to 3}}}})

  (advance-all-elevators) ; We advance one tick on the clock
  (is= @elevators {0 {:id            0,
                      :state         :working,
                      :floor         1,
                      :rides-waiting #{},
                      :rides-active  #{{:from 1, :to 4}}},
                   1 {:id            1,
                      :state         :working,
                      :floor         1,
                      :rides-waiting #{{:from 3, :to 2}},
                      :rides-active  #{{:from 0, :to 3}}}})

  (advance-all-elevators)
  (is= @elevators {0 {:id            0,
                      :state         :working,
                      :floor         2,
                      :rides-waiting #{},
                      :rides-active  #{{:from 1, :to 4}}},
                   1 {:id            1,
                      :state         :working,
                      :floor         2,
                      :rides-waiting #{{:from 3, :to 2}},
                      :rides-active  #{{:from 0, :to 3}}}})

  (advance-all-elevators)
  (is= @elevators {0 {:id            0,
                      :state         :working,
                      :floor         3,
                      :rides-waiting #{},
                      :rides-active  #{{:from 1, :to 4}}},
                   1 {:id            1,
                      :state         :working,
                      :floor         3,
                      :rides-waiting #{},
                      :rides-active  #{{:from 3, :to 2}}}})

  (advance-all-elevators)
  (is= @elevators {0 {:id            0,
                      :state         :waiting,
                      :floor         4,
                      :rides-waiting #{},
                      :rides-active  #{}},
                   1 {:id            1,
                      :state         :waiting,
                      :floor         2,
                      :rides-waiting #{},
                      :rides-active  #{}}})


  ;  #todo add in unit tests for above scenario (is= ...)

  )


