(ns demo.core
  (:use tupelo.core)
  (:require
    [clojure.set :as set]
    [clojure.string :as str]
    [schema.core :as s]
    [tupelo.schema :as tsk]
    )
  (:import [clojure.lang Atom]))

; NOTE: internally, floor numbers and elevator ids are zero-based & contiguous

(declare num-floors)
(declare num-elevators)
(declare elevators)

;-----------------------------------------------------------------------------
(def Ride {:from s/Int
           :to   s/Int})

(def Elevator ; #todo make explicit
  {:id            s/Int
   :state         s/Keyword
   :floor         s/Int
  ;:direction     s/Keyword
   :rides-waiting #{Ride}
   :rides-active  #{Ride}})

;-----------------------------------------------------------------------------
(s/defn new-elevator :- Elevator
  "Creates a new elevator with the specified id & default state."
  ([] (new-elevator 0))
  ([arg]
   (cond
     (int? arg) {:id            arg
                 :state         :waiting ; :going-down :going-up :positioning
                 :floor         0
                ;:direction     :none
                 :rides-waiting #{}
                 :rides-active  #{}}
     (map? arg) (glue (new-elevator) arg)
     :else (throw (ex-info "bad arg" arg)))))

(defn init [ctx]
  (with-map-vals ctx [num-elevators num-floors]
    (def num-floors num-floors)
    (def num-elevators num-elevators)
    (def elevators (atom (apply glue
                           (forv [id (range num-elevators)]
                             {id (new-elevator id)}))))) )

(s/defn ride-up? :- s/Bool
  [ride :- Ride]
  (with-map-vals ride [from to]
    (< from to)))

(s/defn ride-down? :- s/Bool
  [ride :- Ride]
  (with-map-vals ride [from to]
    (< to from)))

(s/defn ride-dir :- s/Keyword
  [ride :- Ride]
  (cond
    (ride-up? ride) :up
    (ride-down? ride) :down
    :else :none))

(s/defn elevator-dir :- s/Keyword
  [elevator :- Elevator]
  (let [actives (grab :rides-active elevator)
        uppers  (keep-if #(ride-up? %) actives)
        downers (keep-if #(ride-down? %) actives)]
    (assert (= (count actives)
              (+ (count uppers) (count downers))))
    (cond
      (pos? (count uppers)) :up
      (pos? (count downers)) :down
      :else :none)))

(s/defn same-direction? :- s/Bool
  [elevator :- Elevator
   ride :- Ride]
  (=
    (validate #(not= :none %) (elevator-dir elevator))
    (validate #(not= :none %) (ride-dir ride))))

(defn display-elevators []
  (let [floors     (range num-floors)
        floor-strs (mapv #(format "%02d" %) floors)
        tens-str   (str/join (mapv xfirst floor-strs))
        ones-str   (str/join (mapv xsecond floor-strs))]
    (newline)
    (println (str "   " tens-str))
    (println (str "   " ones-str))
    (doseq [[id elev] @elevators]
      (let [floor              (grab :floor elev)
            direction          (elevator-dir elev)
            rides-up-active    (filterv ride-up? (grab :rides-active elev))
            rides-up-waiting   (filterv ride-up? (grab :rides-waiting elev))
            rides-down-active  (filterv ride-down? (grab :rides-active elev))
            rides-down-waiting (filterv ride-down? (grab :rides-waiting elev))

            dir-char           (cond
                                 (= direction :none) "|"
                                 (= direction :up) ">"
                                 (= direction :down) "<"
                                 :else (throw (ex-info "bad direction " (vals->map elev))))
            output-line        (atom (vec (repeat num-floors "-")))
            mark-floors-fn     (fn [rides symbol]
                                 (doseq [ride rides]
                                   (with-map-vals ride [from to]
                                     (swap! output-line replace-at from symbol)
                                     (swap! output-line replace-at to symbol))))]
        (mark-floors-fn rides-up-active \U)
        (mark-floors-fn rides-up-waiting \u)
        (mark-floors-fn rides-down-active \D)
        (mark-floors-fn rides-down-waiting \d)
        (swap! output-line replace-at floor dir-char)
        (let [elev-str (str/join @output-line)]
          (println (format "%2d %s" id elev-str)))))))

(s/defn find-farthest-start :- s/Int
  [elevator :- Elevator
   rides :- #{Ride}]
  (let [start-floors (mapv #(grab :from %) rides)
        farthest     (if (= :up (elevator-dir elevator))
                       (apply max start-floors)
                       (apply min start-floors))]
    farthest))

(s/defn update-elevator :- Elevator
  [elevator :- Elevator
   event :- tsk/KeyMap ]
  (let [state-curr (grab :state elevator)
        floor-curr (grab :floor elevator) ]
    (cond ;-----------------------------------------------------------------------------
      (= state-curr :waiting)
      (cond
        (= (grab :type event) :new-ride) (let [ride (grab :ride event)]
                                           (if (= floor-curr (grab :from ride))
                                             ; Then: ride starts from present position
                                             (glue elevator
                                               {:state        :working
                                                :rides-active #{ride}})
                                             ; Else; use synthetic ride to reposition for new ride
                                             (let [synth-ride {:from floor-curr :to (grab :from ride)}]
                                               (glue elevator
                                                 {:state         :working
                                                  :rides-waiting #{ride}
                                                  :rides-active  #{synth-ride}}))))
        (= (grab :type event) :tick) elevator
        :else (throw (ex-info "invalid :waiting event" (vals->map event elevator)))
        ) ;-----------------------------------------------------------------------------
      (= state-curr :working)
      (cond
        (= (grab :type event) :new-ride) (let [ride (grab :ride event)]
                                           (glue elevator {:rides-waiting #{ride}}))
        (= (grab :type event) :tick) (let [elev-dir           (validate #(not= :none %) (elevator-dir elevator))
                                           floor-curr         (grab :floor elevator)
                                           floor-next         (if (= :up elev-dir)
                                                                (inc floor-curr)
                                                                (dec floor-curr))
                                           rides-active       (grab :rides-active elevator)
                                           rides-waiting      (grab :rides-waiting elevator)
                                           rides-ending       (keep-if #(= floor-next (grab :to %)) rides-active)
                                           rides-starting     (keep-if #(= floor-next (grab :from %))
                                                                (keep-if #(same-direction? elevator %) rides-waiting))
                                           rides-active-next  (it-> rides-active
                                                                (set/union it rides-starting)
                                                                (set/difference it rides-ending))
                                           rides-waiting-next (set/difference rides-waiting rides-starting)
                                           state-next         (if (empty? rides-active-next)
                                                                :waiting
                                                                :working)
                                           elevator-next      (glue elevator {:state         state-next
                                                                              :rides-active  rides-active-next
                                                                              :rides-waiting rides-waiting-next
                                                                              :floor         floor-next})]
                                       ; elevator may have no more waiting rides in same dir
                                       (if (not-empty? rides-active-next)
                                         elevator-next
                                         ; else (empty? rides-active-next)
                                         (if (not-empty? rides-waiting-next)
                                           (let [floor-farthest (find-farthest-start elevator rides-waiting-next)]
                                             (if (= floor-next floor-farthest)
                                               ; start all trips at floor-farthest
                                               (let [starting-now         (keep-if #(= floor-farthest (grab :from %)) rides-waiting-next)
                                                     rides-active-next-2  starting-now
                                                     rides-waiting-next-2 (set/difference rides-waiting-next starting-now)
                                                     elevator-next-2      (glue elevator-next {:rides-active  rides-active-next-2
                                                                                               :rides-waiting rides-waiting-next-2
                                                                                               :state         :working})]
                                                 elevator-next-2)
                                               (glue elevator-next {:state        :working
                                                                    :rides-active {:from floor-next :to floor-farthest}})))
                                           (glue elevator-next {:state :waiting}))))
        :else (throw (ex-info "invalid :working event" (vals->map event elevator)))
        ) ;-----------------------------------------------------------------------------


      :else (throw (ex-info "invalid state" (vals->map elevator)))
      ))
  )

(s/defn add-ride
  [elev-id :- s/Int
   floor-from :- s/Int
   floor-to :- s/Int]
  (assert (contains? (set (keys @elevators)) elev-id))
  (assert (not= floor-from floor-to))
  (swap! elevators (fn [elevs]
                     (update-in elevs [elev-id]
                       #(update-elevator % {:type  :new-ride
                                            :ride {:from floor-from :to floor-to}})))))

(defn advance-all-elevators []
  (swap! elevators map-vals #(update-elevator % {:type :tick})))












